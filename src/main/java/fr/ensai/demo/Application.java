package fr.ensai.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "fr.ensai.demo")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    // Si vous avez des beans spécifiques à configurer, vous pouvez les ajouter ici. Par exemple :
    // @Bean
    // public ModelMapper modelMapper() {
    //     return new ModelMapper();
    // }
}
