package fr.ensai.demo.service;

public abstract class ScanTemplateMethod {

    // Template method defining the skeleton of a scan algorithm
    public final void performScan() {
        preScanHook();
        scanOperation();
        postScanHook();
    }

    // Hook for pre-scan operations, which can be overridden by subclasses
    protected void preScanHook() {
        // Default pre-scan actions, if any
    }

    // Abstract method for the main scanning operation, to be implemented by subclasses
    protected abstract void scanOperation();

    // Hook for post-scan operations, which can be overridden by subclasses
    protected void postScanHook() {
        // Default post-scan actions, if any
    }
}
