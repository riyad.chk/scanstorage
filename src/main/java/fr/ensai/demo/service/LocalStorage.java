package fr.ensai.demo.service;

import fr.ensai.demo.dto.ScanConfiguration;
import fr.ensai.demo.model.FileComponent;
import fr.ensai.demo.model.FolderComposite;
import java.io.File;

public class LocalStorage extends ScanService {

    private ScanConfiguration configuration;

    @Override
    protected void authenticateStorage() {
        // Pas d'authentification nécessaire pour le stockage local.
    }

    @Override
    protected void startScan() {
        System.out.println("Début du scan local dans le dossier : " + configuration.getRootPath());
    }

    @Override
    protected void processData() {
        File rootDirectory = new File(configuration.getRootPath());
        FolderComposite rootFolder = new FolderComposite(rootDirectory);
        rootFolder.scan();
    }

    @Override
    protected void storeData() {
        // Ici, vous pourriez implémenter la logique pour stocker les résultats du scan, si nécessaire.
    }

    @Override
    protected void endScan() {
        System.out.println("Scan local terminé.");
    }

    @Override
    public void configure(ScanConfiguration configuration) {
        this.configuration = configuration;
    }
}
