package fr.ensai.demo.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import fr.ensai.demo.dto.ScanConfiguration;

public class S3Storage extends ScanService {

    private final AmazonS3 s3Client;
    private final String bucketName;

    public S3Storage(String bucketName) {
        this.s3Client = AmazonS3ClientBuilder.standard().build();
        this.bucketName = bucketName;
    }

    @Override
    protected void authenticateStorage() {
        // L'authentification est gérée par l'AWS SDK lors de la création du client
    }

    @Override
    protected void startScan() {
        System.out.println("Début du scan du bucket S3: " + bucketName);
    }

    @Override
    protected void processData() {
        ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName);
        ListObjectsV2Result result;

        do {
            result = s3Client.listObjectsV2(req);

            for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
                System.out.println(" - " + objectSummary.getKey() + "  " +
                                   "(size = " + objectSummary.getSize() + ")");
                // Traitement supplémentaire ici
            }
            req.setContinuationToken(result.getNextContinuationToken());
        } while(result.isTruncated());
    }

    @Override
    protected void storeData() {
        // Stockage des résultats du scan, si nécessaire
    }

    @Override
    protected void endScan() {
        System.out.println("Scan terminé pour le bucket S3: " + bucketName);
    }

    @Override
    public void configure(ScanConfiguration scanConfiguration) {
        this.scanConfiguration = scanConfiguration;
        // Initialisation spécifique à S3, par exemple, définir le bucket S3 cible
    }

}
