package fr.ensai.demo.service;

import fr.ensai.demo.dto.ScanConfiguration;
public abstract class ScanService {
    
    // Ajoutez un champ pour la configuration
    protected ScanConfiguration scanConfiguration;

    // La méthode template qui définit le squelette de l'algorithme de scan.
    public final void scanAndStoreData() {
        authenticateStorage();
        startScan();
        processData();
        storeData();
        endScan();
    }

    // Méthode pour configurer le service de scan
    public abstract void configure(ScanConfiguration scanConfiguration);

    // Étapes qui seront implémentées différemment selon le type de stockage.
    protected abstract void authenticateStorage();
    protected abstract void startScan();
    protected abstract void processData();
    protected abstract void storeData();
    protected abstract void endScan();
    
}
