package fr.ensai.demo.dto;


public class ScanConfiguration {
    private int maxDepth;
    private int maxFiles;
    private String fileNameFilter;
    private String fileTypeFilter;
    private String rootPath; // Declare rootPath as a private field

    // Constructeur par défaut
    public ScanConfiguration() {
    }

    // Constructeur avec tous les paramètres
    public ScanConfiguration(int maxDepth, int maxFiles, String fileNameFilter, String fileTypeFilter, String rootPath) {
        this.maxDepth = maxDepth;
        this.maxFiles = maxFiles;
        this.fileNameFilter = fileNameFilter;
        this.fileTypeFilter = fileTypeFilter;
        this.rootPath = rootPath;
    }

    // Getters et setters

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }


    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public int getMaxFiles() {
        return maxFiles;
    }

    public void setMaxFiles(int maxFiles) {
        this.maxFiles = maxFiles;
    }

    public String getFileNameFilter() {
        return fileNameFilter;
    }

    public void setFileNameFilter(String fileNameFilter) {
        this.fileNameFilter = fileNameFilter;
    }

    public String getFileTypeFilter() {
        return fileTypeFilter;
    }

    public void setFileTypeFilter(String fileTypeFilter) {
        this.fileTypeFilter = fileTypeFilter;
    }

    // toString, hashCode, et equals méthodes si nécessaire
}
