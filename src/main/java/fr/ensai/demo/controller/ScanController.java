package fr.ensai.demo.controller;

import fr.ensai.demo.dto.ScanConfiguration;
import fr.ensai.demo.service.LocalStorage;
import fr.ensai.demo.service.S3Storage;
import fr.ensai.demo.service.ScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/scans")
public class ScanController {

    private final LocalStorage localStorageService;
    private final S3Storage s3StorageService;

    @Autowired
    public ScanController(LocalStorage localStorageService, S3Storage s3StorageService) {
        this.localStorageService = localStorageService;
        this.s3StorageService = s3StorageService;
    }

    @PostMapping("/local")
    public ResponseEntity<String> executeLocalScan(@RequestBody ScanConfiguration configuration) {
        localStorageService.configure(configuration); // Méthode fictive pour configurer le service avant le scan
        localStorageService.scanAndStoreData();
        return ResponseEntity.ok("Local scan executed successfully.");
    }

    @PostMapping("/s3")
    public ResponseEntity<String> executeS3Scan(@RequestBody ScanConfiguration configuration) {
        s3StorageService.configure(configuration); // Méthode fictive pour configurer le service avant le scan
        s3StorageService.scanAndStoreData();
        return ResponseEntity.ok("S3 scan executed successfully.");
    }
}
