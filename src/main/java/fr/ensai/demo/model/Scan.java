package fr.ensai.demo.model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "scan")
public class Scan {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "max_files")
    private int maxFiles;

    @Column(name = "max_depth")
    private int maxDepth;

    @Column(name = "file_name_filter")
    private String fileNameFilter;

    @Column(name = "file_type_filter")
    private String fileTypeFilter;

    @Column(name = "scan_date")
    private Date scanDate;

    @Column(name = "execution_time")
    private long executionTime;

    // Getters and setters for each field

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getMaxFiles() {
        return maxFiles;
    }

    public void setMaxFiles(int maxFiles) {
        this.maxFiles = maxFiles;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public String getFileNameFilter() {
        return fileNameFilter;
    }

    public void setFileNameFilter(String fileNameFilter) {
        this.fileNameFilter = fileNameFilter;
    }

    public String getFileTypeFilter() {
        return fileTypeFilter;
    }

    public void setFileTypeFilter(String fileTypeFilter) {
        this.fileTypeFilter = fileTypeFilter;
    }

    public Date getScanDate() {
        return scanDate;
    }

    public void setScanDate(Date scanDate) {
        this.scanDate = scanDate;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
    }
    
    // Default constructor, hashCode, equals, and toString methods as needed
}
