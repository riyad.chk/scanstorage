package fr.ensai.demo.model;

import java.io.File;

public class FileLeaf implements FileComponent {
    private final File file;

    public FileLeaf(File file) {
        this.file = file;
    }

    @Override
    public void scan() {
        // Logique pour traiter un fichier individuel ici.
        System.out.println("Scanning file: " + file.getAbsolutePath());
    }
}
