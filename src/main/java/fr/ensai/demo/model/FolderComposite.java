package fr.ensai.demo.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FolderComposite implements FileComponent {
    private final List<FileComponent> children = new ArrayList<>();
    private final File directory;

    public FolderComposite(File directory) {
        this.directory = directory;
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                children.add(new FolderComposite(file));
            } else {
                children.add(new FileLeaf(file));
            }
        }
    }

    @Override
    public void scan() {
        // Logique pour scanner un dossier et ses sous-dossiers ici.
        System.out.println("Scanning directory: " + directory.getAbsolutePath());
        for (FileComponent child : children) {
            child.scan();
        }
    }
}
