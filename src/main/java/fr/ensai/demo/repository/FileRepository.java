package fr.ensai.demo.repository;

import fr.ensai.demo.model.FileComponent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface FileRepository extends JpaRepository<FileComponent, Long> {
    // Trouve tous les composants de fichier liés à un scan spécifique
    List<FileComponent> findByScanId(UUID scanId);

    // Trouve tous les composants de fichier par type
    List<FileComponent> findByType(String type);

    // Supprime tous les composants de fichier liés à un scan spécifique
    void deleteByScanId(UUID scanId);

    // Trouve un composant de fichier par son chemin
    FileComponent findByPath(String path);

    // Trouve tous les composants de fichier dans un répertoire spécifique
    List<FileComponent> findByParentId(Long parentId);
}
