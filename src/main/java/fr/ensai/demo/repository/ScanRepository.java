package fr.ensai.demo.repository;

import fr.ensai.demo.model.Scan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ScanRepository extends JpaRepository<Scan, UUID> {
    
    // Trouve tous les scans avec une limite de profondeur spécifique
    List<Scan> findByMaxDepth(int maxDepth);

    // Trouve tous les scans avec un nombre spécifique de fichiers maximaux
    List<Scan> findByMaxFiles(int maxFiles);

    // Trouve tous les scans qui contiennent un certain type de fichier
    @Query("SELECT s FROM Scan s WHERE s.fileTypeFilter = :fileType")
    List<Scan> findByFileType(String fileType);

    // Trouve tous les scans qui contiennent un certain nom de fichier dans leur filtre
    @Query("SELECT s FROM Scan s WHERE s.fileNameFilter LIKE %:fileName%")
    List<Scan> findByFileNameFilter(String fileName);

    // Supprime un scan par son identifiant
    void deleteById(UUID id);

    // Trouve le scan le plus récent
    @Query("SELECT s FROM Scan s ORDER BY s.scanDate DESC")
    Scan findLatestScan();

    // Calcule la moyenne des temps d'exécution des scans
    @Query("SELECT AVG(s.executionTime) FROM Scan s")
    long calculateAverageExecutionTime();

    // Calcule le nombre total de scans
    @Query("SELECT COUNT(s) FROM Scan s")
    long countTotalScans();
}
