DROP TABLE IF EXISTS file_component;
DROP TABLE IF EXISTS scan;

-- Creates the Scan table to store metadata about each scan operation
CREATE TABLE IF NOT EXISTS scan (
    id UUID PRIMARY KEY,
    max_files INT NOT NULL,
    max_depth INT NOT NULL,
    file_name_filter VARCHAR(255),
    file_type_filter VARCHAR(255),
    scan_date TIMESTAMP NOT NULL,
    execution_time BIGINT NOT NULL
);

-- Creates the FileComponent table to represent each file or directory encountered during a scan
CREATE TABLE IF NOT EXISTS file_component (
    id BIGSERIAL PRIMARY KEY,
    scan_id UUID NOT NULL,
    name VARCHAR(255) NOT NULL,
    size BIGINT NOT NULL,
    last_modified TIMESTAMP NOT NULL,
    path VARCHAR(255) NOT NULL,
    type VARCHAR(50) NOT NULL,
    parent_id BIGSERIAL,
    CONSTRAINT fk_file_component_scan FOREIGN KEY (scan_id) REFERENCES scan (id),
    CONSTRAINT fk_file_component_parent FOREIGN KEY (parent_id) REFERENCES file_component (id)
);
